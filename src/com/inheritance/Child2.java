package com.inheritance;

public class Child2 extends Parent{
	void property3() {
		System.out.println("Child2 Property3");
	}
	public static void main(String[] args) {
		Parent p=new Parent();
		p.Property1();
		System.out.println();
		Child c1=new Child();
		c1.Property1();
		c1.property2();
		System.out.println();
		Child2 c2=new Child2();
		c2.Property1();
		c2.property3();
	}

}

